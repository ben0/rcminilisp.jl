## IterateWithPrev iterator wrapper ##

struct ThereIsNoPreviousItem end

"""
Iterator over `(previous item, current item)` pairs.

Has the same length as `inner_itr`.  For the first such pair, `previous_item`
is equal to the sentinel `ThereIsNoPreviousItem()`.
"""
struct IterateWithPrev
    inner_itr
end

struct _ThereIsNoPreviousState end
function Base.iterate(itr::IterateWithPrev, state=_ThereIsNoPreviousState())
    if state isa _ThereIsNoPreviousState
        prev_inner_item = ThereIsNoPreviousItem()
        inner_result = Base.iterate(itr.inner_itr)
    else
        (prev_inner_item, inner_state) = state
        inner_result = Base.iterate(itr.inner_itr, inner_state)
    end
    isnothing(inner_result) && return nothing
    (inner_item, next_inner_state) = inner_result
    item = (prev_inner_item, inner_item)
    next_state = (inner_item, next_inner_state)
    return (item, next_state)
end

Base.length(itr::IterateWithPrev) = Base.length(itr.inner_itr)

Base.isempty(itr::IterateWithPrev) = Base.isempty(itr.inner_itr)
