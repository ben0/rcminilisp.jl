import Automa
import Automa.RegExp: RE, @re_str
import DataStructures: OrderedDict

const re = Automa.RegExp

Token = @NamedTuple{kind::Symbol, text::String}

function _with_named_exit!(regex::RE, name::Symbol)
    regex.actions[:exit] = [name]
    return regex
end

### Microsyntax declaration ###

const whitespace1 = re"[ \t\n]"
const identifier_char = re"[^()\";]" \ whitespace1

const keywords = (:if, :begin)

lexical_categories = OrderedDict(
    name => _with_named_exit!(regex, name)
    for (name, regex) in [
        :paren_open   => re"\(",
        :paren_close  => re"\)",
        # Each keyword gets its own lexical category, and the name of the
        # category is the keyword
        (kw           => re.primitive(string(kw))
                          for kw in keywords)...,
        :comment      => re";[^\n]*",
        :int          => re"-?[0-9]+",
        :float        => (re"-?[0-9]+\.[0-9]*?"
                          | re"-?\.[0-9]+"),
        # Multi-line strings are allowed.  Any character may be escaped by a
        # preceding backslash.  Backslashes and double-quote marks must be
        # escaped.
        :string       => re"\"([^\"\\]|\\.)*\"",
        :identifier   => ((identifier_char \ re"[0-9]")
                          * re.rep(identifier_char)),
        :whitespace   => re.rep(whitespace1),
    ])

### Build/load function ###

"""
Initialize the global tokenizer (by compiling the DFA).

This function creates the global function `tokenize(data::String)`, which
returns a tuple `(tokens, i_last)` where:

* `tokens` is a list of (lexical category, text) pairs
* `i_last` is the index into `data` of the last character that is part of an
  emitted token.  If the scanner consumes the whole string, then this will be
  equal to `lastindex(data)`; otherwise, it will be less.

If scanning fails (i.e., if a microsyntax error is encountered), then this
function throws an error.

Note: Once this package and the language it defines become more stable, it
should be possible to store the generated code inside the package tree.  Seems
like this could be done via a simple build script and git hook and/or CI.

This implementation is based heavily on `test09` [1] (the example code on the
Automa docs site seems to be a little more stale/buggy).

[1] https://github.com/BioJulia/Automa.jl/blob/46d6d336b576fd2dbc25b84e587d797478436631/test/test09.jl#L15-L27
"""
function compile_tokenizer!()
    global _tokenizer = Automa.compile(
        (regex => :(emit(Symbol($(string(name))), ts:te))
         for (name, regex) in lexical_categories)...)

    global tokenize
    ctx = Automa.CodeGenContext()
    # The funtion body below refers to the following variables that are managed
    # by the generated code (see docs [1]):
    #
    #     `p`: current position of data
    # `p_end`: end position of data
    # `p_eof`: end position of file stream
    #    `ts`: start position of token
    #    `te`: end position of token
    #    `cs`: current state
    #
    # [1] https://biojulia.net/Automa.jl/latest/references.html#Automa.Variables
    @eval (function tokenize(data::String)
               $(Automa.generate_init_code(ctx, _tokenizer))
               p_end = p_eof = sizeof(data)
               tokens = Token[]
               emit(kind, range) = push!(tokens, (kind=kind, text=data[range]))
               while p ≤ p_eof && cs > 0
                   $(Automa.generate_exec_code(ctx, _tokenizer))
               end
               if cs < 0
                   scan_error(string("Scan error near position ", p))
               end
               return (tokens, te)
           end)
end
