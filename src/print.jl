"""
Pretty-printer for a sequence of tokens.  Prints one line per token, unless the
textual representation of the token spans multiple lines.
"""
_strlen(s::Symbol) = length(string(s))
const max_cat_strlen = maximum(_strlen(cat)
                               for cat in keys(lexical_categories))
function print_tokens(io::IO, tokens)
    for token in tokens
        println(io,
                repeat(" ", max_cat_strlen - _strlen(token.kind)),
                "[$(token.kind)]    $(token.text)")
    end
end

print_tokens(tokens) = print_tokens(stdout, tokens)


"""
Pretty-printer for a parsed expression.
"""
function print_expr(io::IO, expr)
    function print_indented(e, indent_first::Integer, indent_hanging::Integer)
        if e isa Token
            print(io, " "^indent_first, "$(e.text)   [$(e.kind)]")
        else
            @assert e isa Vector
            if isempty(e)
                @warn("Didn't expect to see an empty combination, are you"
                      * " sure this is a valid expression?")
                print(io, " "^indent_first, "()")
            else
                print(io, " "^indent_first, "(")
                print_indented(e[1], 0, indent_hanging + 1)
                println(io)
                for i in 2:lastindex(e)
                    print_indented(e[i], indent_hanging + 2, indent_hanging + 2)
                    i == lastindex(e) || println(io)
                end
                print(io, ")")
            end
        end
    end
    print_indented(expr, 0, 0)
    println(io)
end

print_expr(expr) = print_expr(stdout, expr)
