## Scan error type ##

struct RScanError <: Exception
    msg::String
end

Base.showerror(io::IO, e::RScanError) = print(io, "RScanError: ", e.msg)

scan_error(msg::AbstractString) = throw(RScanError(msg))


## Parse error type ##

struct RParseError <: Exception
    msg::String
end

Base.showerror(io::IO, e::RParseError) = print(io, "RParseError: ", e.msg)

parse_error(msg::AbstractString) = throw(RParseError(msg))
