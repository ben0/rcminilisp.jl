"""
Parse a sequence of `Token`s into a list representing a single Lisp expression.

For documentation of what token "kinds" (lexical categories) exist, see
`scan.jl`.
"""
function parse(tokens)
    if isempty(tokens)
        parse_error("Empty token sequence is not a valid expression")
    end
    # We consume tokens in a single pass.  We remember the previous token just
    # to detect the case where two tokens run into each other that should have
    # a space in between (e.g., a string and an identifier).
    tokens_itr = enumerate(IterateWithPrev(Iterators.Stateful(tokens)))

    # Skip initial whitespace and comments
    i = 0
    token = (kind=:_uninitialized, text="")
    while true
        # Working around what I think is a bug in `isempty(::Enumerate)`
        # https://discourse.julialang.org/t/isempty-enumerate-iterators-stateful-advances-the-stateful-iterator/71475
        box = first(tokens_itr, 1)
        isempty(box) && parse_error("All-whitespace-or-comment token sequence"
                                    * " is not a valid expression")
        (i, (_, token)) = only(box)
        token.kind ∈ (:whitespace, :comment) || break
    end
    # Certain token categories are always invalid for the first non-skipped
    # token
    if token.kind == :paren_close
        parse_error("unmatched close paren at token $i")
    elseif token.kind ∈ keywords
        parse_error("Keyword occurred outside a combination at token $i")
    # If the first non-skipped token is not an open paren, then it has to be a
    # single-token expression and it has to be the only non-skipped token.
    elseif token.kind ∈ (:int, :float, :string, :identifier)
        if any(token_.kind ∉ (:whitespace, :comment)
               for (i_, (_, token_)) in tokens_itr)
            parse_error("Two expressions juxtaposed outside a combination near"
                        * " token $i")
        end
        return token
    end

    @assert token.kind == :paren_open
    # `tree` is the parse tree we will return
    tree = Any[]
    # `stack` contains the sequence of combinations in which our cursor is
    # currently nested, from outermost to innermost
    stack = Any[tree]
    for (i, (prev_token, token)) in tokens_itr
        if (!isa(prev_token, ThereIsNoPreviousItem)
                && !_neighboring_tokens_legal(prev_token.kind, token.kind))
            parse_error("Token categories `$(prev_token.kind)` and"
                        * " `$(token.kind)` cannot appear next to each other,"
                        * " please add a space at token $i")
        end
        if isempty(stack)
            # The top-level combination is finished, expect to see only
            # comments and whitespace from here on
            all(token_.kind ∈ (:comment, :whitespace)
                for (i_, (_, token_)) in tokens_itr) || parse_error(
                    "Trailing (non-skipped) tokens outside the top-level"
                    * " combination after token $i")
            break
        end
        if token.kind ∈ (:comment, :whitespace)
            # nothing to do here
        elseif token.kind == :paren_open
            comb = Any[]
            push!(stack[end], comb)
            push!(stack, comb)
        elseif token.kind == :paren_close
            err = _check_combination_shallow(stack[end])
            isnothing(err) || parse_error(
                    "Invalid combination ending at token $i: $err")
            pop!(stack)
        elseif token.kind ∈ (:int, :float, :string, :identifier, keywords...)
            push!(stack[end], token)
        else
            parse_error("Unhandled category :$(token.kind), please report bug")
        end
    end
    if !isempty(stack)
        parse_error("Unfinished combination (not enough close parens)")
    end
    return tree
end


"""
Simple (and incomplete) validator for a parsed combination.

If validation passes, returns `nothing`.  If validation fails, returns
explanation text for the first validation failure found.
"""
function _check_combination_shallow(comb::Vector)
    if isempty(comb)
        return "Empty combination is invalid. Looking for `(list)`?"
    end
    head = comb[1]
    args = comb[2:end]
    if any(arg isa Token && arg.kind ∈ keywords
           for arg in args)
        return "Keyword can only occur at the head of a combination"
    elseif head isa Token && head.kind ∈ (:int, :float, :string)
        return ("Lexical category `$(head.kind)` cannot occur at the head of"
                * " a combination")
    elseif head isa Token && head.kind == :if && length(args) != 3
        return ("`if` must have exactly 3 arguments:"
                * " `(if condition v_true v_false)`")
    end
    return nothing
end

# Require spaces between certain kinds of tokens, to avoid typos.  These
# choices are somewhat aesthetic and do not agree with MIT/GNU Scheme.
const _touchy_kinds = (:identifier, :string, :int, :float, keywords...)
function _neighboring_tokens_legal(kind1::Symbol, kind2::Symbol)
    return !(kind1 ∈ _touchy_kinds && kind2 ∈ _touchy_kinds
             || (kind1 ∉ (:paren_open, :whitespace)
                 && kind2 == :paren_open)
             || (kind1 == :paren_close
                 && kind2 ∉ (:paren_close, :whitespace)))
end
