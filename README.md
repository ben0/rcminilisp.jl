# RCMiniLisp.jl

A WIP Lisp interpreter for the Recurse Center pair programming interview.  The
Lisp dialect of this WIP interpreter is a subset of Scheme.

## Overview

This was my first time writing a program that cleanly separates scanning
(lexical analysis) and parsing.  Before/while writing this, I read the chapters
on scanning and parsing in Cooper and Torczon, _Engineering a Compiler_.

### Scanning

The source file `src/scan.jl` declares the lexical categories and their
corresponding regular expressions, converts those declarations into a form for
which [Automa.jl](https://biojulia.net/Automa.jl/latest/) can compile a DFA,
and exposes a method `tokenize(::String)` which uses the generated DFA code to
scan input text.

In this package, a "token" is a `@NamedTuple{kind::Symbol, text::String}` like
`(kind=:int, text="20")`.  The return value of `tokenize` is a list of tokens,
and can be pretty-printed with the function `print_tokens`:
```julia
data = """(repeat (+ 4 5) "hello")  ; An enthusiastic greeting"""
(tokens, i_last) = tokenize(data)
i_last == lastindex(data) || error(
       "Scanner didn't consume the whole string (unfinished lexeme)")
print_tokens(tokens)
```

This outputs

```
 [paren_open]    (
 [identifier]    repeat
 [whitespace]     
 [paren_open]    (
 [identifier]    +
 [whitespace]     
        [int]    4
 [whitespace]     
        [int]    5
[paren_close]    )
 [whitespace]     
     [string]    "hello"
[paren_close]    )
 [whitespace]      
    [comment]    ; An enthusiastic greeting
```

### Parsing

It is common to generate a parser automatically from an EBNF or other grammar
description.  However, I was not able to find a mature parser generator that
outputs Julia code, so I wrote the parser by hand in `src/parse.jl`.  Luckily,
among modern programming languages, Scheme is one of the easiest to parse.

The method `parse(tokens)` parses the given token sequence (which is assumed to
represent a single expression) into an AST, or throws an error if the token
sequence is invalid.  The AST for an expression is either a single token, or a
`Vector` whose elements are either tokens or ASTs for sub-expressions.  The AST
can be pretty-printed by the function `print_expr`.  With `tokens` defined as
above,

```julia
print_expr(parse(tokens))
```

outputs

```
(repeat   [identifier]
  (+   [identifier]
    4   [int]
    5   [int])
  "hello"   [string])
```

### Testing

As usual, a test suite can be found in `test/runtests.jl`.

### Fiddle notebooks

My typical setup for developing Julia code is to write the library code in a
package and test out invocations of the library functions in a Jupyter
notebook.  Essentially, I use the notebook as a recorded, browser-based REPL
with nicer formatting and the ability to display images inline.
[Revise.jl](https://github.com/timholy/Revise.jl) allows me to edit the package
code without having to reload the Jupyter kernel (most of the time).  The
Jupyter extension [Jupytext](https://github.com/mwouts/jupytext) saves these
notebooks as Julia scripts which I then check in to version control under the
subdirectory `fiddle/`.  The name "fiddle" comes from an
[episode](https://clojuredesign.club/episode/014-fiddle-with-the-repl/) of the
podcast Functional Design in Clojure, which describes the practice of using
"fiddle files" in a similar way.
