# ---
# jupyter:
#   jupytext:
#     formats: ipynb,jl:light
#     text_representation:
#       extension: .jl
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.1
#   kernelspec:
#     display_name: Julia 1.6.2
#     language: julia
#     name: julia-1.6
# ---

# ## Setup

import Pkg
Pkg.activate(".")

import Revise
import RCMiniLisp as R

R.compile_tokenizer!()

# ## Tokenizer smoke-test

data = read("scan_examples/ex1.txt", String)
(tokens, i_last) = R.tokenize(data)
i_last == lastindex(data) || error(
    "Scanner didn't consume the whole string (unfinished lexeme)")
R.print_tokens(tokens)

R.tokenize("")

# ## Parser smoke test

data = """  (+ 1 (* 3 4)
              5
              (if #t 6  ; how about "unmatched" parens and quotes in a comment ))  "
                 7))
     ; hello there
"""
(tokens, i_last) = R.tokenize(data)
i_last == lastindex(data) || error(
    "Scanner didn't consume the whole string (unfinished lexeme)")
R.print_tokens(tokens)

R.print_expr(R.parse(tokens))

# ## DFA visualization

import Automa

let d = mktempdir()
    dotpath = joinpath(d, "machine.dot")
    svgpath = joinpath(d, "machine.svg")
    write(dotpath, Automa.machine2dot(R._tokenizer.machine))
    run(`dot -Tsvg -o "$svgpath" "$dotpath"`)
    display("image/svg+xml", String(read(svgpath)))
end
