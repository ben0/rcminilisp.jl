import RCMiniLisp as R
import Test: @testset, @test, @test_throws

R.compile_tokenizer!()

const lparen = (kind=:paren_open, text="(")
const rparen = (kind=:paren_close, text=")")
const t_if = (kind=:if, text="if")
const t_begin = (kind=:begin, text="begin")
ident(text::String) = (kind=:identifier, text=text)
space(text::String) = (kind=:whitespace, text=text)
t_int(text::String) = (kind=:int, text=text)
t_str(text::String) = (kind=:string, text=text)
t_comment(text::String) = (kind=:comment, text=text)

include("scan.jl")
include("parse.jl")
include("util.jl")
