struct IncompleteScanError <: Exception end

function tokenize_checkall(data::String)
    (tokens, i_last) = R.tokenize(data)
    i_last == lastindex(data) || throw(IncompleteScanError())
    return tokens
end

function parse_checkall(data::String)
    return R.parse(tokenize_checkall(data))
end

@testset "`if`" begin
    @test (parse_checkall("(if condition t_yes t_no)")
           == [t_if, ident("condition"),
                 ident("t_yes"),
                 ident("t_no")])

    @test (parse_checkall("(if (= 1 2) t_yes (+ 3 4))")
           == [t_if, [ident("="), t_int("1"), t_int("2")],
                 ident("t_yes"),
                 [ident("+"), t_int("3"), t_int("4")]])

    @test_throws R.RParseError parse_checkall("(if)")

    @test_throws R.RParseError parse_checkall("""(if condition
                                                   t_yes
                                                   t_no
                                                   t_maybe_so)""")
end

@testset "`begin`" begin
    @test parse_checkall("(begin)") == [t_begin]
    @test parse_checkall("(begin 1)") == [t_begin, t_int("1")]
    @test parse_checkall("(begin (a) 7)") == [t_begin, [ident("a")],
                                              t_int("7")]
end

@testset "Comments" begin
    @test_throws R.RParseError parse_checkall("(f x  ; y)")
    @test_throws R.RParseError parse_checkall("""(f x  ; ))
                                                         ))
                                                   y)""")
    @test parse_checkall("""(f x  ; )) "
                              y)""") == [ident("f"), ident("x"), ident("y")]
end

@testset "Strings with escaping" begin
    @test (parse_checkall(read("escaping/escaped_quote.txt", String))
           == t_str("\"a\\\"b\""))

    @test_throws IncompleteScanError parse_checkall(
            read("escaping/escaped_backslash_unescaped_quote.txt", String))

    @test (parse_checkall(read(
                "escaping/escaped_backslash_escaped_quote.txt", String))
           == t_str("\"a\\\\\\\"b\""))

    @test_throws R.RParseError parse_checkall(
            read("escaping/escaped_backslash_juxtaposed_expr.txt", String))

    @test (parse_checkall(read(
                "escaping/unnecessary_escape.txt", String))
           == t_str("\"a\\b\""))
end

@testset "Spacing" begin
    @test parse_checkall("""(; hello"
                              a)""") == [ident("a")]

    @test parse_checkall("  a   ") == ident("a")
    @test parse_checkall("a;7") == ident("a")
    @test parse_checkall("a;") == ident("a")
    @test parse_checkall("""(a; 7
                              )""") == [ident("a")]

    @test_throws R.RParseError parse_checkall(""" a"b" """)
    @test_throws R.RParseError parse_checkall(""" "a"b """)
    @test_throws R.RParseError parse_checkall("3.0a")
    @test_throws R.RParseError parse_checkall("3a")

    @test_throws R.RParseError parse_checkall("(a(b c))")
    @test_throws R.RParseError parse_checkall("((a b)c)")
end

@testset "Zero expressions or more than one expression" begin
    @test_throws R.RParseError parse_checkall("")
    @test_throws R.RParseError parse_checkall(" ")
    @test_throws R.RParseError parse_checkall("   \t ")
    @test_throws R.RParseError parse_checkall("  ; hello")
    @test_throws R.RParseError parse_checkall("1 2")
    @test_throws R.RParseError parse_checkall("(a) (b c)")
end
