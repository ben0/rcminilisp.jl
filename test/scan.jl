@testset "Scanning a lively one-liner" begin
    data = "))(\t hi!\\if)begin;\""
    (tokens, i_last) = R.tokenize(data)
    @test i_last == lastindex(data)
    @test tokens == [rparen, rparen, lparen, space("\t "), ident("hi!\\if"),
                     rparen, t_begin, t_comment(";\"")]
end

@testset "Scanning a comment that has parens and quotes in it" begin
    data = """
           a "b"
             ; )"
           c"""
    (tokens, i_last) = R.tokenize(data)
    @test i_last == lastindex(data)
    @test tokens == [ident("a"), space(" "), t_str("\"b\""), space("\n  "),
                     t_comment("; )\""), space("\n"), ident("c")]
end

@testset "Scanning a multi-line string that looks like a comment but isn't" begin
    data = """
           a "b
             ; )"
           c"""
    (tokens, i_last) = R.tokenize(data)
    @test i_last == lastindex(data)
    @test tokens == [ident("a"), space(" "), t_str("\"b\n  ; )\""),
                     space("\n"), ident("c")]
end

@testset "Scanning escaped backslash followed by unescaped quote" begin
    data = read("escaping/escaped_backslash_unescaped_quote.txt", String)
    (tokens, i_last) = R.tokenize(data)
    @test i_last < lastindex(data)
    @test i_last == findlast(==('\"'), data) - 1
    @test tokens == [t_str("\"a\\\\\""), ident("b")]
end
