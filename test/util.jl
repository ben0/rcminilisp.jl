@testset "`IterateWithPrev` on empty iterator" begin
    itr = R.IterateWithPrev(Iterators.Stateful([]))
    @test collect(itr) == []
end

@testset "`IterateWithPrev` on singleton iterator" begin
    itr = R.IterateWithPrev(Iterators.Stateful(["one"]))
    @test collect(itr) == [(R.ThereIsNoPreviousItem(), "one")]
end

@testset "`IterateWithPrev` on a tripleton iterator" begin
    itr = R.IterateWithPrev(Iterators.Stateful(["one", "two", "three"]))
    @test collect(itr) == [(R.ThereIsNoPreviousItem(), "one"),
                           ("one", "two"), ("two", "three")]
end
